import React from 'react'
import { Input } from 'antd'
import queryString from 'query-string';
const { Search } = Input;
function SearchMobie() {
    const onSearch = (value) => {
        console.log(value);
        const queryParams = queryString.stringify({tenPhim : value}) ; 
        window.location.search = queryParams ;
        
    }
    return (
        <div>
            <Search className='mx-auto block mb-3 shadow' style={{width : '95%'}} size='large' placeholder="Nhập phim vào đây ..." onSearch={onSearch} enterButton />
        </div>
    )
}

export default SearchMobie