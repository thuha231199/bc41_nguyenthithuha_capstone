import React from 'react'
import { Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { handleOk } from '../../Toolkits/formSuccessSlice';
import { CheckCircleOutlined } from '@ant-design/icons';
function SuccessForm() {
    let dispatch = useDispatch() ; 
    let {isModalVisible} = useSelector(state => state.formSuccessSlice) ; 
    let handleOkModal = () => {
        dispatch(handleOk()) ; 
    }
    return (
        <div className='flex justify-center items-center'>
        {/* Your register form here */}
        <Modal
            className='text-center'
            open={isModalVisible}
            onOk={handleOkModal}
            onCancel={handleOkModal}
            footer={null}
        >
            <div className="success-message space-y-5">
                <CheckCircleOutlined style={{ fontSize: '100px', color: '#52c41a' }} />
                <p className='text-2xl font-bold'>Đăng ký thành công.</p>
                <small style={{color : '#2a9d8f'}} className='text-sm font-bold'>Vui lòng chờ trong giây lát để chuyển tới trang đăng nhập...</small>
            </div>
        </Modal>
        </div>
    );
}

export default SuccessForm