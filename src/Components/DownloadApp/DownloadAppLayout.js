import React from 'react'
import { Desktop, LargeDesktop, Mobile, Tablet } from '../../Layout/Responsive'
import DownloadAppDesktop from './DownloadAppDesktop'
import DownloadAppMobile from './DownloadAppMobile'
import DownloadAppTablet from './DownloadAppTablet'

export default function DownloadAppLayout() {
  return (
    <div>
      <Mobile>
        <DownloadAppMobile/>        
      </Mobile>
      <Tablet>
        <DownloadAppTablet/>
      </Tablet>
      <Desktop>
        <DownloadAppDesktop/>
      </Desktop>
      <LargeDesktop>
        <DownloadAppDesktop/>
      </LargeDesktop>
    </div>
  )
}
