import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { articleServ } from '../../Services/articlesService';

export default function TabsNewsMobTab() {
  const [articleDienAnh, setArticleDienAnh] = useState([]);
  const [articleReview, setArticleReview] = useState([]);
  const [articleKhuyenMai, setArticleKhuyenMai] = useState([]);
  const [seeMore, setSeeMore] = useState(false);
 
  const onClickSeeMore = () => {
    articleServ.getArticleDienAnh()
                  .then((res) => {
                        setArticleDienAnh(res.data);
                        setSeeMore(true);
                  })
                  .catch((err) => {
                        console.log(err);
                  });
  }
  const onClickSeeLess = () => {
    setSeeMore(false);
  };
  
  let renderArticle = (article) => {
    return (
      <>
        <div className='articleItem'>
            <a href={article[0].url} className='space-y-2'>
              <img src={article[0].img} alt="" />
              <h5>{article[0].title}</h5>
              <p>{article[0].text}</p>
            </a>
          </div>
          <div className='articleItem'>
            <a href={article[1].url} className='space-y-2'>
              <img src={article[1].img} alt="" />
              <h5>{article[1].title}</h5>
              <p>{article[1].text}</p>
            </a>
          </div>
          <div className='articleItem '>
            <a href={article[2].url} className='space-y-2'>
              <img src={article[2].img} alt="" />
              <h5>{article[2].title}</h5>
              <p>{article[2].text}</p>
            </a>
          </div>
          <div className='articleItem '>
            <a href={article[3].url} className='space-y-2'>
              <img src={article[3].img} alt="" />
              <h5>{article[3].title}</h5>
              <p>{article[3].text}</p>
            </a>
          </div>
          <div className='articleItem grid grid-flow-row text-left gap-2'>
            <div className='articleItem_small'>
              <a href={article[4].url} >
                <div className='space-x-2 flex justify-start items-start'>
                  <img src={article[4].img} alt="" style={{width:50}}/>
                  <h5>{article[4].title} </h5>
                </div>
              </a>
            </div>
            <div className='articleItem_small'>
              <a href={article[5].url} >
                <div className='space-x-2 flex justify-start items-start'>
                  <img src={article[5].img} alt="" style={{width:50}}/>
                  <h5>{article[5].title} </h5>
                </div>
              </a>
            </div>
            <div className='articleItem_small'>
              <a href={article[6].url} >
                <div className='space-x-2 flex justify-start items-start'>
                  <img src={article[6].img} alt="" style={{width:50}}/>
                  <h5>{article[6].title} </h5>
                </div>
              </a>
            </div>
            <div className='articleItem_small'>
              <a href={article[7].url} >
                <div className='space-x-2 flex justify-start items-start'>
                  <img src={article[7].img} alt="" style={{width:50}}/>
                  <h5>{article[7].title} </h5>
                </div>
              </a>
            </div>
          </div>
      </>
    )
  }
  const items = [
    {
        key: '1',
        label: `Điện Ảnh 24h`,
        children: (<div className='articleContent mt-1 text-center'>
                    <div className='grid grid-flow-row gap-3 mb-3'>{seeMore && articleDienAnh && renderArticle(articleDienAnh)}</div>
                    <button onClick={seeMore ? onClickSeeLess : onClickSeeMore}>
                    {seeMore ? "RÚT GỌN" : "XEM THÊM"}
                    </button>
                   </div>),
    },
    {
        key: '2',
        label: `Review`,
        children: (<div className='articleContent mt-1 text-center'>
                      <div className='grid grid-flow-row gap-3 mb-3'>{seeMore && articleDienAnh && renderArticle(articleDienAnh)}</div>
                      <button onClick={seeMore ? onClickSeeLess : onClickSeeMore} id='newsButton'>
                      {seeMore ? "RÚT GỌN" : "XEM THÊM"}
                      </button>
                    </div>),
    },
    {
        key: '3',
        label: `Khuyến mãi`,
        children: (<div className='articleContent mt-1 text-center'>
                      <div className='grid grid-flow-row gap-3 mb-3'>{seeMore && articleDienAnh && renderArticle(articleDienAnh)}</div>
                      <button onClick={seeMore ? onClickSeeLess : onClickSeeMore} id='newsButton'>
                      {seeMore ? "RÚT GỌN" : "XEM THÊM"}
                      </button>
                    </div>),
    },
  ];
  
  return (
    <div id='tabsNews' className='mt-10 mb-16 overflow-x-hidden'>
        <div className="container flex flex-col items-center justify-center space-y-5">
            <Tabs defaultActiveKey="1" items={items}/>
        </div>
    </div>
  )
}
