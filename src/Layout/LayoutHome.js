import React from 'react'
import Footer from '../Components/Footer/Footer'
import Headers from '../Components/Header/Header'

export default function LayoutHome({Component}) {
  return (
    <div className='h-full min-h-screen flex flex-col'>
        <Headers/>
        {/* Chứa: banner , danh sách phim , tab rạp phim */}
        <div className='flex-grow mt-0'>
          <Component/>
        </div>
        <Footer/>
    </div>
  )
}
