import React from 'react'
import Carousel from '../../Components/Carousel/Carousel'
import TabsMovieLayout from '../../Components/TabsMovie/TabsMovieLayout'
import MovieList from '../../Components/MovieList/MovieList'
import Modal from '../../Components/Modal/Modal'
import DownloadAppLayout from '../../Components/DownloadApp/DownloadAppLayout'
import TabsNewsLayout from '../../Components/TabsNews/TabsNewsLayout'

function HomePage() {
  return (
    <div>
      <Modal/>
      <Carousel/>
      <MovieList/>
      <TabsMovieLayout/>
      <TabsNewsLayout/>
      <DownloadAppLayout/>
    </div>
  )
}

export default HomePage