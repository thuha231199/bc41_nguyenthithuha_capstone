// TODO: file dùng để validate các ô input trong form đăng ký 
// * Ô tài khoản : gồm 6 ký tự trở lên , bao gồm chữ và số 
import { message } from "antd";
import validator from "validator";

export const validateTk = (taiKhoan) => {
    let flag = false;
    if (validator.matches(taiKhoan, /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/)) {
        flag = true;
    } else {
        message.error('Gồm 6 ký tự: chữ và số')
    }
    return flag;
}

// * Mật khẩu
export const validateMk = (matKhau) => {
    let flag = false;
    const { isLength } = require('validator');
    const isValidPassword = isLength(matKhau.trim(), { min: 6 });
    if (isValidPassword) {
        flag = true;
    }else {
        message.error('Mật khẩu ít nhất 6 kí tự') ; 
    }
    return flag;
}

// * Kiểm tra mật khẩu
export const validateEqualMk = (matKhau , matKhauNhapLai) => {
    let flag = false;
    const { equals } = require('validator');
    const arePasswordsEqual = equals(matKhau, matKhauNhapLai);
    if (arePasswordsEqual) {
        flag = true;
    }else {
        message.error('Mật khẩu không trùng khớp') ; 
    }
    return flag;
}

// * Kiểm tra họ Tên : chử không chứa ký tự đặc biệt

export const validateHt = (hoTen) => {
    let flag = false;
    if (validator.isLength(hoTen, { min: 3, max: 50 }) && validator.matches(hoTen, /^[\p{L} ]+$/u)) {
        flag = true;
    } else {
        message.error('Họ tên không hợp lệ');
    }
    return flag
}

// * Kiểm tra email : hợp lệ abc@gmail.com  

export const validateEmail = (email) => {
    let flag = false ; 
    const validator = require('validator');
    // Kiểm tra email
    if (validator.isEmail(email)) {
    flag = true;
    } else {
    message.error('Email không hợp lệ') ; 
    }
    return flag ; 
}

// * Kiểm tra số diện thoại : số điện thoại theo chuẩn Việt Nam quy định gồm 10 số 

export const validatePhoneVietNam = (soDt) => {
    let flag = false;
    const validator = require('validator');
// Kiểm tra số điện thoại ở Việt Nam
    if (validator.isMobilePhone(soDt, 'vi-VN')) {
        flag = true;
    } else {
        message.error('Số điện thoại không hợp lệ') ; 
    }
    return flag ; 
} 