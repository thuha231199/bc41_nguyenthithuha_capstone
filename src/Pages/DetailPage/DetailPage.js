import React from 'react'
import Headers from '../../Components/Header/Header'
import ModalPlayer from '../../Components/Modal/Modal'

function DetailPage({Component}) {
    return (
        <div className='w-full h-full'>
            <ModalPlayer/>
            <Headers/>
            <Component/>
        </div>
    )
}

export default DetailPage