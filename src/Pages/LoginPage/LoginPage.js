import React from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { userServ } from '../../Services/userService';
import { useDispatch } from 'react-redux';
import { setLoginUser } from '../../Toolkits/userSlice';
import { localUserServ } from '../../Services/localService';

function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let fillForm = () => {
    let info = localUserServ.get();
    if (info != null) {
      console.log(info);
      return info;
    }
    else {
      return {taiKhoan: "", matKhau: ""}
    }
  };
  const onFinish = (values) => {
    userServ.loginUser(values)
            .then((res) => {
              message.success("Đăng nhập thành công");
              dispatch(setLoginUser(res.data.content));
              localUserServ.set(res.data.content);
              setTimeout(() => {
                navigate('/') ;
              } , 1500) ;
            })
            .catch((err) => {
              message.error("Đăng nhập thất bại");
              console.log(err);
            })
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  
  return (
    <div className='loginPage text-2xl bg-white rounded-xl p-5 sm:w-2/3 md:w-2/5 lg:w-1/3'>
      <div className='loginTitle text-center w-full space-y-5 mb-10'>
        <i style={{lineHeight : "45px" , borderRadius : '50%'}} className="fas fa-user fa-lg fa-fw bg-gray-500 w-12 h-12 text-white text-2xl"></i>
        <h2 className='text-3xl font-bold text-black-700 '>Đăng nhập</h2>
      </div>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 24,
        }}
        style={{
          maxWidth: 600,
        }}
        initialValues={{
          remember: true,
          taiKhoan: fillForm().taiKhoan,
          matKhau: fillForm().matKhau,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout='vertical'
        className='space-y-8'
      >
        <Form.Item
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Trường hợp bắt buộc',
            },
          ]}
          
        >
          <Input className='py-3 text-gray-800 hover:border-black-600 focus:border-black' placeholder="Tài Khoản *"/>
        </Form.Item>

        <Form.Item
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Trường hợp bắt buộc',
            },
          ]}
        >
          <Input.Password className='py-3 text-gray-800 hover:border-black focus:border-white' placeholder="Mật Khẩu *"/>
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Checkbox>Nhớ tài khoản</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
        >
          <Button type="primary" htmlType="submit" className='loginButton bg-black text-white mb-3 py-6 block w-full hover:text-gray-500 bg-gray  '
          >
            Đăng nhập
          </Button>
          <NavLink to={'/register'} className="text-right block hover:underline underline-offset-2 decoration-black hover:decoration-black">
          <span className='text-black font-medium'>Bạn chưa có tài khoản? Đăng ký</span>
          </NavLink>
        </Form.Item>
      </Form>
    </div>
  )
}

export default LoginPage