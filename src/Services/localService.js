export const USER_INFO = "USER_INFO" ; 
export const localUserServ = {
    get : () => {
        let jsonData = localStorage.getItem(USER_INFO) ; 
        return JSON.parse(jsonData) ? JSON.parse(jsonData) : null ; 
    } , 
    set : (userInfo) => {
        let jsonData = JSON.stringify(userInfo) ; 
        localStorage.setItem(USER_INFO , jsonData) ; 
    } , 
    remove : () => {
        localStorage.removeItem(USER_INFO) ; 
    }
}